package com.afterTrainingCode.HospitalManagement.utl;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class MailUtil {
    String recipientMail ;
    String recipientName;
    String subject;
    String messageBody;
}
