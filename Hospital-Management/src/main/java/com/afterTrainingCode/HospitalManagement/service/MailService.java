package com.afterTrainingCode.HospitalManagement.service;

import com.afterTrainingCode.HospitalManagement.utl.MailUtil;

import javax.mail.MessagingException;

public interface MailService {
    public String sendSimpleEmail(MailUtil mailUtil) throws MessagingException;
    public String sendHtmlEmail(MailUtil mailUtil, String htmlBody) throws MessagingException;
    public String sendMessageUsingTemplateEmail(MailUtil mailUtil) throws MessagingException;
}
