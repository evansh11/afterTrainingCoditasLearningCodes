package com.afterTrainingCode.HospitalManagement.service;

import com.afterTrainingCode.HospitalManagement.entities.Doctor;
import com.afterTrainingCode.HospitalManagement.model.request.AddDoctorDto;
import com.afterTrainingCode.HospitalManagement.model.request.AdmitPatientDto;
import com.afterTrainingCode.HospitalManagement.model.request.UpdateMyDoctorDto;
import com.afterTrainingCode.HospitalManagement.model.response.CustomResponseDTO;
import com.afterTrainingCode.HospitalManagement.model.response.PatientResponseDto;

import javax.mail.MessagingException;

public interface DoctorService {
    Doctor addDoctor(AddDoctorDto addDoctorDto );

    CustomResponseDTO updateMyDoctor(UpdateMyDoctorDto updateMyDoctor) throws MessagingException;
}
