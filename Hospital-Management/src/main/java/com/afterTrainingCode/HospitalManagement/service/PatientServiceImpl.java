package com.afterTrainingCode.HospitalManagement.service;

import com.afterTrainingCode.HospitalManagement.entities.Doctor;
import com.afterTrainingCode.HospitalManagement.entities.Patient;
import com.afterTrainingCode.HospitalManagement.entities.Room;
import com.afterTrainingCode.HospitalManagement.exception.DoctorNotFoundException;
import com.afterTrainingCode.HospitalManagement.exception.PatientNotFoundException;
import com.afterTrainingCode.HospitalManagement.exception.VacantRoomNotFoundException;
import com.afterTrainingCode.HospitalManagement.model.request.AdmitPatientDto;
import com.afterTrainingCode.HospitalManagement.model.response.CustomResponseDTO;
import com.afterTrainingCode.HospitalManagement.model.response.PatientResponseDto;
import com.afterTrainingCode.HospitalManagement.repository.DoctorRepository;
import com.afterTrainingCode.HospitalManagement.repository.PatientRepository;
import com.afterTrainingCode.HospitalManagement.repository.RoomRepository;
import com.afterTrainingCode.HospitalManagement.utl.MailUtil;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class PatientServiceImpl implements PatientService {

    @Autowired
    private PatientRepository patientRepository;
    @Autowired
    private RoomRepository roomRepository;
    @Autowired
    private DoctorRepository doctorRepository;

    @Autowired
    ModelMapper mapper;
    @Autowired
    MailService mailService;


    @Override
    public PatientResponseDto registerPatient(Patient patientDto) throws MessagingException {
        Patient bookAppointmentForNewPatient = new Patient();
        //setting data from dto to patient
        this.mapper.map(patientDto, bookAppointmentForNewPatient);
        PatientResponseDto responseDto = new PatientResponseDto();
        this.mapper.map(patientRepository.save(bookAppointmentForNewPatient), responseDto);

        //sending mail
        MailUtil mail = new MailUtil();
        mail.setRecipientMail(patientDto.getUserEmail());
        mail.setSubject("PATIENT REGISTERED !");
        mail.setRecipientName(patientDto.getPatientName());
        mail.setMessageBody("Dear patient , you have been registered successfully !");
        mailService.sendMessageUsingTemplateEmail(mail);

        //sending response
        return responseDto;
    }

    @Override
    public PatientResponseDto admitPatient(AdmitPatientDto patientDto) throws MessagingException {
        Patient patient = patientRepository.findById(patientDto.getPatientId()).get();
        // if patient not registered
        if (patient == null)
            throw new PatientNotFoundException();
        // checking room availability
        Room vacantRoom = roomRepository.checkVacantRoom(patientDto.getRoomType());
        if (vacantRoom == null)
            throw new VacantRoomNotFoundException();
        else {
            patient.setRoom(vacantRoom);
            vacantRoom.setRoomStatus(false);
            vacantRoom.setPatient(patient);
            patientRepository.save(patient);
        }
        //sending mail
        MailUtil mail = new MailUtil();
        mail.setSubject("ROOM BOOKED !");
        mail.setMessageBody("You have got your room successfully !");
        mail.setRecipientMail(patient.getUserEmail());
        mail.setRecipientName(patient.getPatientName());
        mailService.sendMessageUsingTemplateEmail(mail);

        //response
        PatientResponseDto response = new PatientResponseDto();
        this.mapper.map(patient, response);
        this.mapper.map(vacantRoom, response);
        return response;
    }

    @Override
    public PatientResponseDto assignDoctor(Long patientId) throws MessagingException {
        Doctor yourDoctor;
        Patient patient = patientRepository.findById(patientId).get();
        // if patient not registered
        if (patient == null)
            throw new PatientNotFoundException();
        //checking available doctors
        yourDoctor = doctorRepository.checkAvailableDoctor();
        if (yourDoctor == null)
            throw new DoctorNotFoundException();

        //assigning doctor
        patient.setDoctor(yourDoctor);
        yourDoctor.setStatus(false);
        patientRepository.save(patient);

        //setting response
        PatientResponseDto response = new PatientResponseDto();
        this.mapper.map(patient, response);
        this.mapper.map(yourDoctor, response);

        //sending email
        MailUtil mail = new MailUtil();
        mail.setRecipientName(response.getPatientName());
        mail.setSubject("DOCTOR ASSIGNED !");
        mail.setMessageBody("Dear patient your doctor is assigned : "+response.getDoctorName()+" !");
        mail.setRecipientMail(patient.getUserEmail());
        mailService.sendMessageUsingTemplateEmail(mail);
        return response;
    }
}
