package com.afterTrainingCode.HospitalManagement.service;

import com.afterTrainingCode.HospitalManagement.entities.Doctor;
import com.afterTrainingCode.HospitalManagement.entities.Patient;
import com.afterTrainingCode.HospitalManagement.exception.DoctorNotAvailableException;
import com.afterTrainingCode.HospitalManagement.exception.DoctorNotFoundException;
import com.afterTrainingCode.HospitalManagement.exception.PatientNotFoundException;
import com.afterTrainingCode.HospitalManagement.model.request.AddDoctorDto;
import com.afterTrainingCode.HospitalManagement.model.request.UpdateMyDoctorDto;
import com.afterTrainingCode.HospitalManagement.model.response.CustomResponseDTO;
import com.afterTrainingCode.HospitalManagement.model.response.PatientResponseDto;
import com.afterTrainingCode.HospitalManagement.repository.DoctorRepository;
import com.afterTrainingCode.HospitalManagement.repository.PatientRepository;
import com.afterTrainingCode.HospitalManagement.utl.MailUtil;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;

@Service
public class DoctorServiceImpl implements DoctorService {
    @Autowired
    DoctorRepository doctorRepository;

    @Autowired
    PatientRepository patientRepository;
    @Autowired
    ModelMapper mapper;
    @Autowired
    MailService mailService;

    @Override
    public Doctor addDoctor(AddDoctorDto addDoctorDto) {
        Doctor addDoctor = new Doctor();
        this.mapper.map(addDoctorDto, addDoctor);
        return doctorRepository.save(addDoctor);
    }

    @Override
    public CustomResponseDTO updateMyDoctor(UpdateMyDoctorDto updateMyDoctor) throws MessagingException {
        //checking that patient exist or not
        Patient patient = patientRepository.findById(updateMyDoctor.getPatientId()).orElse(null);
        if(patient == null)
            throw new PatientNotFoundException();
//            return new CustomResponseDTO("NOT FOUND !",404,"PATIENT NOT FOUND !",null);
        // checking that updatedDoctor available or not
        Doctor updatedDoctor = doctorRepository.findById(updateMyDoctor.getDoctorId()).orElse(null);
        if(updatedDoctor == null)
            return new CustomResponseDTO("NOT FOUND !",404,"DOCTOR NOT FOUND !",null);
        if(! updatedDoctor.isStatus())
            return new CustomResponseDTO("NOT ACCEPTABLE !",406,"DOCTOR NOT AVAILABLE !",null);
        else{
            // setting status free -> true for previous doctor so that doctor status -> free/available
            doctorRepository.findById(patient.getDoctor().getDoctorId()).get().setStatus(true);
            patient.setDoctor(updatedDoctor);

            // setting new doctor status busy
            updatedDoctor.setStatus(false);
            patientRepository.save(patient);
        }

        //setting response
        PatientResponseDto response = new PatientResponseDto();
        this.mapper.map(patient,response);
        this.mapper.map(updatedDoctor,response);

        //sending mail
        MailUtil mail = new MailUtil();
        mail.setRecipientMail(patient.getUserEmail());
        mail.setSubject("DOCTOR CHANGED !");
        mail.setMessageBody("Dear patient your doctor has been changed as per your request !" +
                "\n your new Doctor : "+updatedDoctor.getDoctorName()+" .");
        mail.setRecipientName(patient.getPatientName());
        mailService.sendMessageUsingTemplateEmail(mail);
        return new CustomResponseDTO("STATUS.OK",200,"YOUR DOCTOR UPDATED !",response);
    }
}
