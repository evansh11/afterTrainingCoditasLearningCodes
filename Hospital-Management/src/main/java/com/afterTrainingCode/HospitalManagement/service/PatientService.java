package com.afterTrainingCode.HospitalManagement.service;

import com.afterTrainingCode.HospitalManagement.entities.Patient;
import com.afterTrainingCode.HospitalManagement.model.request.AdmitPatientDto;
import com.afterTrainingCode.HospitalManagement.model.response.PatientResponseDto;

import javax.mail.MessagingException;

public interface PatientService {
    PatientResponseDto registerPatient(Patient patientDto) throws MessagingException;
    PatientResponseDto admitPatient(AdmitPatientDto patientDto) throws MessagingException;
    PatientResponseDto assignDoctor(Long patientId) throws MessagingException;
}
