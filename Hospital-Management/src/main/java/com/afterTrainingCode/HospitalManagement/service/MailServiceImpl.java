package com.afterTrainingCode.HospitalManagement.service;

import com.afterTrainingCode.HospitalManagement.utl.MailUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.HashMap;
import java.util.Map;

@Service
public class MailServiceImpl implements MailService {
    @Autowired
    JavaMailSender mailSender;
    @Value("${spring.mail.username}")
    String serviceMail;

    @Autowired
    SpringTemplateEngine templateEngine;

    @Override
    public String sendSimpleEmail(MailUtil mailUtil) throws MessagingException {
        return null;
    }

    @Override
    public String sendHtmlEmail(MailUtil mailUtil ,String htmlBody) throws MessagingException {
        MimeMessage message =mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message , true ,"UTF-8");
        helper.setFrom(serviceMail);
        helper.setTo(mailUtil.getRecipientMail());
        helper.setSubject(mailUtil.getSubject());
        helper.setText(htmlBody,true);
        mailSender.send(message);
        return "mailSent";
    }

    @Override
    public String sendMessageUsingTemplateEmail(MailUtil mailUtil) throws MessagingException {
        Context context = new Context();
        Map<String,Object> template = new HashMap<>();
        template.put("username" ,mailUtil.getRecipientName());
        template.put("messageBody" ,mailUtil.getMessageBody());
        context.setVariables(template);
        String htmlBody = templateEngine.process("notificationEmail",context);
        sendHtmlEmail(mailUtil,htmlBody);
        return "mailSent";
    }
}
