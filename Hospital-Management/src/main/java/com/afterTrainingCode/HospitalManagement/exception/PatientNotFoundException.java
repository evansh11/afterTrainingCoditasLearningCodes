package com.afterTrainingCode.HospitalManagement.exception;

public class PatientNotFoundException extends RuntimeException{
    public PatientNotFoundException(){
        super("PATIENT NOT FOUND !");
    }
}
