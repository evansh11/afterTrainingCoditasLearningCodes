package com.afterTrainingCode.HospitalManagement.exception;

public class DoctorNotFoundException extends RuntimeException{
    public DoctorNotFoundException(){
        super("DOCTOR NOT FOUND !");
    }
}
