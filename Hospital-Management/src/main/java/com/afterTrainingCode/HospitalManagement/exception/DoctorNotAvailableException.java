package com.afterTrainingCode.HospitalManagement.exception;

public class DoctorNotAvailableException extends RuntimeException{
    public DoctorNotAvailableException(){
        super("DOCTORS ARE NOT AVAILABLE !");
    }
}
