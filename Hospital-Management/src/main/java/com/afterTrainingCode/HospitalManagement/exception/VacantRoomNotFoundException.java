package com.afterTrainingCode.HospitalManagement.exception;

public class VacantRoomNotFoundException extends RuntimeException{
    public VacantRoomNotFoundException(){
        super("ROOMs ARE NOT AVAILABLE !");
    }
}
