package com.afterTrainingCode.HospitalManagement.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(PatientNotFoundException.class)
    public ResponseEntity<Object> patientNotFoundResponse(PatientNotFoundException exception , WebRequest webRequest){
        ErrorResponse response = new ErrorResponse(exception.getMessage());
        return handleExceptionInternal(exception,response,null, HttpStatus.NOT_FOUND,webRequest);
    }

    @ExceptionHandler(VacantRoomNotFoundException.class)
    public ResponseEntity<Object> vacantRoomNotFoundResponse(Exception exception , WebRequest webRequest){
        ErrorResponse response = new ErrorResponse(exception.getMessage());
        return handleExceptionInternal(exception,response,null, HttpStatus.NOT_FOUND,webRequest);
    }
    @ExceptionHandler(DoctorNotFoundException.class)
    public ResponseEntity<Object> doctorNotFoundExceptionResponse(Exception exception , WebRequest webRequest){
        ErrorResponse response = new ErrorResponse(exception.getMessage());
        return handleExceptionInternal(exception,response,null, HttpStatus.NOT_FOUND,webRequest);
    }
    @ExceptionHandler(DoctorNotAvailableException.class)
    public ResponseEntity<Object> doctorNotAvailableExceptionResponse(Exception exception , WebRequest webRequest){
        ErrorResponse response = new ErrorResponse(exception.getMessage());
        return handleExceptionInternal(exception,response,null, HttpStatus.NOT_FOUND,webRequest);
    }
}
