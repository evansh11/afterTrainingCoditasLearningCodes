package com.afterTrainingCode.HospitalManagement.model.request;

import lombok.Data;

@Data
public class AdmitPatientDto {
    private Long patientId;
    private String roomType;
}
