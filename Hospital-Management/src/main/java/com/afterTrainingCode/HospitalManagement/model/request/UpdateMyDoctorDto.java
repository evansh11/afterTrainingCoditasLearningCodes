package com.afterTrainingCode.HospitalManagement.model.request;

import lombok.Data;

@Data
public class UpdateMyDoctorDto {
    private Long patientId;
    private Long doctorId;
}
