package com.afterTrainingCode.HospitalManagement.model.response;

import lombok.Data;

@Data
public class PatientResponseDto {
    private Long patientId;
    private String patientName;
    private int age;
    private Long roomId;
    private Long doctorId;
    private String doctorName;
}
