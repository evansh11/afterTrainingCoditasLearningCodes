package com.afterTrainingCode.HospitalManagement.model.request;

import lombok.Data;
@Data
public class AddDoctorDto {
    private String doctorName;
    private boolean status;
}
