package com.afterTrainingCode.HospitalManagement.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CustomResponseDTO {
    private String status;
    private Integer statusCode;
    private String message ;
    private Object data;
}
