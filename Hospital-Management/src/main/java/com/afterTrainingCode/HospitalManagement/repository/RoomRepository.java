package com.afterTrainingCode.HospitalManagement.repository;

import com.afterTrainingCode.HospitalManagement.entities.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
@Repository
public interface RoomRepository extends JpaRepository<Room,Long> {
    @Query(value = " SELECT * FROM ROOM_TABLE WHERE ROOM_AVAILABILITY = TRUE AND TYPE= ?1 LIMIT 1" ,nativeQuery = true)
    Room checkVacantRoom(String roomType);
}
