package com.afterTrainingCode.HospitalManagement.repository;

import com.afterTrainingCode.HospitalManagement.entities.Doctor;
import com.afterTrainingCode.HospitalManagement.entities.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface DoctorRepository extends JpaRepository<Doctor,Long> {
    @Query(value = " SELECT * FROM DOCTOR_TABLE WHERE AVAILABILITY = TRUE LIMIT 1" ,nativeQuery = true)
    Doctor checkAvailableDoctor();
}
