package com.afterTrainingCode.HospitalManagement.repository;

import com.afterTrainingCode.HospitalManagement.entities.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PatientRepository extends JpaRepository<Patient,Long> {
}
