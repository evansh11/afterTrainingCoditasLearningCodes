package com.afterTrainingCode.HospitalManagement.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ModelMapperInstance {
    @Bean
    public ModelMapper getModelMapper(){
        return new ModelMapper();
    }
}
