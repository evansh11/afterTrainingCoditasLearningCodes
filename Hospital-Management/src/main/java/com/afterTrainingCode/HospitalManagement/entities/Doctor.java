package com.afterTrainingCode.HospitalManagement.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "doctor_table")
public class Doctor {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE ,generator ="doctorSequence" )
    @SequenceGenerator(name = "doctorSequence",sequenceName = "doctorSequence", initialValue = 114)
    @Column(name = "doctorId")
    private Long doctorId;

    @Column(name = "doctorName" ,nullable = false)
    private String doctorName;

/*    @Column(name = "availability" ,columnDefinition = "true") this doesn't work
      private boolean status = ture -> this also doesn't work
 */
    @Column(name = "availability" )
    private boolean status ;

    //one doctor can see multiple patients
    @JsonIgnore
    @OneToMany(mappedBy = "doctor" )
    private List<Patient> patients;

}
