package com.afterTrainingCode.HospitalManagement.entities;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "room_table")
public class Room {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long roomId;

    @Column(name = "room_availability" )
    private boolean roomStatus;

    @Column(name = "type" ,nullable = false)
    private String roomType;

    @Column(name = "price" , nullable = false)
    private Double roomPricing;

    //one room can have only one patient
    @OneToOne
    @JoinColumn(name = "patientId")
    private Patient patient;

}
