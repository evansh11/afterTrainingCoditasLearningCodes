package com.afterTrainingCode.HospitalManagement.entities;

import lombok.Data;
import  java.util.List;
import javax.persistence.*;

@Data
@Entity
@Table(name = "hospital_table")
public class Hospital {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "hospitalId")
    private Long hospitalId;

    @Column(name = "hospitalName" ,nullable = false)
    private String hospitalName;

}
