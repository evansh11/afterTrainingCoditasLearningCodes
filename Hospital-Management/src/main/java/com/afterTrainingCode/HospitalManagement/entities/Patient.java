package com.afterTrainingCode.HospitalManagement.entities;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;

@Entity
@Data
@Table(name = "patient_table")
public class Patient {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "patientSequence")
    @SequenceGenerator(name = "patientSequence" ,sequenceName = "patientSequence" ,initialValue = 1006)
    @Column(name = "patientId")
    private Long patientId;

    @Column(name = "patientName" , nullable = false)
    private String patientName;

    @Column(name = "patientAge")
    private int age;

    //adding email as extra field
    @Column(name = "userEmail")
    @Email(regexp = "[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}",
            flags = Pattern.Flag.CASE_INSENSITIVE)
    private String userEmail;

    // one room can be assigned to one patient only
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "roomId")
    private Room room;

    //
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "doctorId")
    private Doctor doctor;
}
