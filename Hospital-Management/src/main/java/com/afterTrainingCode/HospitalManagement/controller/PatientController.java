package com.afterTrainingCode.HospitalManagement.controller;

import com.afterTrainingCode.HospitalManagement.entities.Patient;
import com.afterTrainingCode.HospitalManagement.model.request.AdmitPatientDto;
import com.afterTrainingCode.HospitalManagement.model.response.PatientResponseDto;
import com.afterTrainingCode.HospitalManagement.service.PatientService;
import com.afterTrainingCode.HospitalManagement.service.PatientServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/patient")
public class PatientController {
    @Autowired
    PatientServiceImpl patientService;

    @PostMapping("/register")
    ResponseEntity registerPatientController(@RequestBody Patient registerPatient) {
        try {
            PatientResponseDto responseDto = patientService.registerPatient(registerPatient);
            if (responseDto != null)
                return new ResponseEntity(responseDto, HttpStatus.CREATED);
            else
                return new ResponseEntity("NOT REGISTERED !", HttpStatus.NOT_ACCEPTABLE);
        }catch (Exception e ){
            return new ResponseEntity("SOMETHING WENT WRONG !",HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PostMapping("/admit")
    ResponseEntity admitPatientController(@RequestBody AdmitPatientDto admitPatientDto) {
        try {
            PatientResponseDto admittedPatient = patientService.admitPatient(admitPatientDto);

            if (admittedPatient != null)
                return new ResponseEntity(admittedPatient, HttpStatus.CREATED);
            else
                return new ResponseEntity("STUDENT NOT ADMITTED !", HttpStatus.NOT_FOUND);

        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity("SOMETHING WENT WRONG !", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @GetMapping("/getDoctor/{patientId}")
    ResponseEntity assignDoctorController(@PathVariable Long patientId) {
        try{
            PatientResponseDto responseDto = patientService.assignDoctor(patientId);
            if(responseDto !=null)
                return new ResponseEntity(responseDto,HttpStatus.OK);
            else
                return new ResponseEntity("DOCTOR NOT AVAILABLE !",HttpStatus.NOT_ACCEPTABLE);
        }
        catch (Exception e ){
            e.printStackTrace();
            return new ResponseEntity("SOMETHING WENT WRONG !",HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
