package com.afterTrainingCode.HospitalManagement.controller;

import com.afterTrainingCode.HospitalManagement.entities.Doctor;
import com.afterTrainingCode.HospitalManagement.model.request.AddDoctorDto;
import com.afterTrainingCode.HospitalManagement.model.request.UpdateMyDoctorDto;
import com.afterTrainingCode.HospitalManagement.model.response.CustomResponseDTO;
import com.afterTrainingCode.HospitalManagement.service.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;

@RestController
@RequestMapping("/doctor")
public class DoctorController {

    @Autowired
    DoctorService doctorService;

    @PostMapping("/addDoctor")
    ResponseEntity addDoctorController(@RequestBody AddDoctorDto addDoctorDto) {
        Doctor addedDoctor = doctorService.addDoctor(addDoctorDto);
        if (addedDoctor != null)
            return new ResponseEntity(addedDoctor, HttpStatus.CREATED);
        else
            return new ResponseEntity("DOCTOR NOT ADDED !", HttpStatus.NOT_ACCEPTABLE);

    }

    @PatchMapping("/updateMyDoctor")
    ResponseEntity updateMyDoctorController(@RequestBody UpdateMyDoctorDto updateMyDoctor) throws MessagingException {
        CustomResponseDTO response = doctorService.updateMyDoctor (updateMyDoctor);
        if (response != null)
            return new ResponseEntity(response, HttpStatus.OK);
        else
            return new ResponseEntity("DOCTOR NOT UPDATED !", HttpStatus.NOT_ACCEPTABLE);

    }
}
