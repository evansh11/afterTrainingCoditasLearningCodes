package com.Springboot.StudentCoureReviewApplication.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateStudentDto {
    private Long studentId;
    private String studentName;
    private String studentCity;
    private String studentEmail;
}
