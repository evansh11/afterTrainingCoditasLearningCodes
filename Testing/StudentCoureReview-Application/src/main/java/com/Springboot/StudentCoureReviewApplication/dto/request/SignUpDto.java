package com.Springboot.StudentCoureReviewApplication.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Id;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SignUpDto {
    private String studentName;
    private String studentCity;
    private String loginEmail;
    private String loginPassword;
    private String loginRole;
}
