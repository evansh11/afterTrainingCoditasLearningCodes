package com.Springboot.StudentCoureReviewApplication.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "Login_info_table")
public class LoginEntity {
    @Id
    @Column(name = "username" , unique = true)
    private String loginEmail;
    @Column(nullable = false ,name = "password" ,unique = true)
    private String loginPassword;
    @Column(nullable = false ,name = "role")
    private String loginRole;
}
