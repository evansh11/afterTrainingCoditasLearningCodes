package com.Springboot.StudentCoureReviewApplication.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReviewDto {
    private Long courseId;
    private Long studentId;
    private Double reviewRating;
    private String reviewDescription;
}
