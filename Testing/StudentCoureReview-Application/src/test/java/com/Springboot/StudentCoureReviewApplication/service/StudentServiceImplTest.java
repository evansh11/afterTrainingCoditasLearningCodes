package com.Springboot.StudentCoureReviewApplication.service;

import com.Springboot.StudentCoureReviewApplication.dto.request.CourseDto;
import com.Springboot.StudentCoureReviewApplication.dto.request.SignUpDto;
import com.Springboot.StudentCoureReviewApplication.dto.request.UpdateStudentDto;
import com.Springboot.StudentCoureReviewApplication.entities.CourseEntity;
import com.Springboot.StudentCoureReviewApplication.entities.LoginEntity;
import com.Springboot.StudentCoureReviewApplication.entities.StudentEntity;
import com.Springboot.StudentCoureReviewApplication.repository.AuthenticationRepository;
import com.Springboot.StudentCoureReviewApplication.repository.CourseRepository;
import com.Springboot.StudentCoureReviewApplication.repository.StudentRepository;
import org.hibernate.internal.build.AllowSysOut;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class StudentServiceImplTest {
    @Mock
    private CourseRepository courseRepository;

    @Mock
    private StudentRepository studentRepository;
    @Mock
    private AuthenticationRepository authenticationRepository;

    @InjectMocks
    private StudentServiceImpl studentService;
    //    @Autowired
    private ModelMapper mapper = new ModelMapper();

    @Test
    @DisplayName("BuyCourseTest")
    void buyCoursesTest() {
        CourseEntity buyCourse = new CourseEntity(1011L, "Junit5", 600.0, new ArrayList<>(), null);
        StudentEntity buyerStudent = new StudentEntity(11056L, "Devansh", "Kanpur", "devansh@Coditas", new ArrayList<>(), null);

        when(courseRepository.findById(Mockito.any())).thenReturn(Optional.of(buyCourse));
        when(studentRepository.findById(Mockito.any())).thenReturn(Optional.of(buyerStudent));
        assertEquals(1, studentService.buyCourses(buyerStudent.getStudentId(), buyCourse.getCourseId()));
    }

//    @Test
//    void registerStudent() {
//        SignUpDto signupStudent = new SignUpDto("Devansh", "Kanpur", "devansh@Codoitas.com", "1111", "student");
//        StudentEntity registeringStudent = new StudentEntity();
//        registeringStudent.setStudentId(1101L);
//        registeringStudent.setStudentEmail(signupStudent.getLoginEmail());
//        registeringStudent.setStudentName(signupStudent.getStudentName());
//        registeringStudent.setStudentCity(signupStudent.getStudentCity());
//
//        LoginEntity loginData = new LoginEntity();
//        loginData.setLoginRole(signupStudent.getLoginRole());
//        loginData.setLoginPassword(signupStudent.getLoginPassword());
//        loginData.setLoginEmail(signupStudent.getLoginEmail());
//
//        when(authenticationRepository.save(loginData)).thenReturn(loginData);
//        when(studentRepository.save(registeringStudent)).thenReturn(registeringStudent);
//
//        assertEquals(registeringStudent, studentService.registerStudent(signupStudent));
//    }

    @Test
    @DisplayName("UpdateStudentTest")
    void updateStudent() {
        StudentEntity student = new StudentEntity(11056L, "Devansh", "Kanpur", "devansh@Coditas", new ArrayList<>(), null);
        UpdateStudentDto updateStudentDto = new UpdateStudentDto(11056L, "Devansh Shamra", "Kanpur", "devansh@Coditas");

        //stubbing
        when(studentRepository.findById(Mockito.any())).thenReturn(Optional.of(student));
        when(studentRepository.save(student)).thenReturn(student);
        StudentEntity updatedStudent = studentService.updateStudent(updateStudentDto);
        assertEquals("Devansh Shamra", updatedStudent.getStudentName());

    }

    @Test
    @DisplayName("deleteStudentTest")
    void deleteStudent() {
        StudentEntity student = new StudentEntity(11056L, "Devansh", "Kanpur", "devansh@Coditas", new ArrayList<>(), null);
        when(studentRepository.findById(student.getStudentId())).thenReturn(Optional.of(student));
        doNothing().when(studentRepository).delete(student);
        assertEquals(true, studentService.deleteStudent(student.getStudentId()));
    }

    @Test
    @DisplayName("showStudentTest")
    void showStudentList() {
        when(studentRepository.findAll()).thenReturn(
                Stream.of(
                        new StudentEntity(11056L, "Devansh", "Kanpur", "devansh@Coditas", new ArrayList<>(), null),
                        new StudentEntity(11058L, "Sabah", "Nagpur", "sabah@Coditas", new ArrayList<>(), null)
                ).collect(Collectors.toList()));
        assertEquals(2, studentService.showStudentList().size());
    }
}