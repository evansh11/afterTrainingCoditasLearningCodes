package com.Springboot.StudentCoureReviewApplication.service;

import com.Springboot.StudentCoureReviewApplication.dto.request.LoginDto;
import com.Springboot.StudentCoureReviewApplication.entities.LoginEntity;
import com.Springboot.StudentCoureReviewApplication.repository.AuthenticationRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AuthenticationServiceImplTest {
    @Mock
    AuthenticationRepository authenticationRepository;

    @Test
    @DisplayName("SignupTest")
    void signUp() {
        LoginEntity user = new LoginEntity();
        user.setLoginRole("student");
        user.setLoginEmail("devansh@Codiats");
        user.setLoginPassword("111111");

        //stubbing
        when(authenticationRepository.save(user)).thenReturn(user);
        assertEquals(user, authenticationRepository.save(user));
    }

    @Test
    @DisplayName("successLoginTest")
    void successLogin() {
        LoginEntity loginUser = new LoginEntity();
        loginUser.setLoginEmail("admin@Coditas");
        loginUser.setLoginPassword("0000");

        //stubbing
        when(authenticationRepository.findById(Mockito.any())).thenReturn(Optional.of(loginUser));
        //checking
        LoginEntity successLogin = authenticationRepository.findById(loginUser.getLoginEmail()).orElse(null);
        assertEquals(true, (successLogin != null) ? true : false);
    }

    @Test
    @DisplayName("WrongUsernameTest")
    void failedLoginDueToWrongUsernameTest() {
        //data coming from form
        LoginDto loginDto = new LoginDto();
        loginDto.setUsername("Admin1@Coditas");
        loginDto.setLoginPassword("0000");
        //actual data
        LoginEntity loginUser = new LoginEntity();
        loginUser.setLoginEmail("admin@Coditas");
        loginUser.setLoginPassword("0000");

        //stubbing
        when(authenticationRepository.findById(Mockito.any())).thenReturn(Optional.of(loginUser));

        //checking username not found scenario
        LoginEntity user = authenticationRepository.findById(loginUser.getLoginEmail()).orElse(null);

        assertEquals(false, user.getLoginEmail().equalsIgnoreCase(loginDto.getUsername()) == false ? false : true);
    }

    @Test
    @DisplayName("WrongPasswordTest")
    void failedLoginWrongPasswordTest() {
        //data coming from form
        LoginDto loginDto = new LoginDto();
        loginDto.setUsername("Admin1@Coditas");
        loginDto.setLoginPassword("0000");
        //actual data
        LoginEntity loginUser = new LoginEntity();
        loginUser.setLoginEmail("admin@Coditas");
        loginUser.setLoginPassword("0001");

        //stubbing
        when(authenticationRepository.findById(Mockito.any())).thenReturn(Optional.of(loginUser));
        //checking
        LoginEntity user = authenticationRepository.findById(loginUser.getLoginEmail()).orElse(null);
        assertEquals(false, (user.getLoginPassword().equals(loginDto.getLoginPassword()) == false) ? false : true);
    }

    @Test
    @DisplayName("RoleTest")
    void roleTest() {
        LoginEntity loginUser = new LoginEntity();
        loginUser.setLoginEmail("admin@Coditas");
        loginUser.setLoginPassword("0001");
        loginUser.setLoginRole("admin");

        //stubbing
        when(authenticationRepository.findById(Mockito.any())).thenReturn(Optional.of(loginUser));
        //checking
        LoginEntity user = authenticationRepository.findById(loginUser.getLoginEmail()).orElse(null);
        assertEquals("admin", user.getLoginRole().equalsIgnoreCase("admin") == true ? "admin" : "student");
    }
}