package com.Springboot.StudentCoureReviewApplication.controller;

import com.Springboot.StudentCoureReviewApplication.dto.request.SignUpDto;
import com.Springboot.StudentCoureReviewApplication.entities.LoginEntity;
import com.Springboot.StudentCoureReviewApplication.service.AuthenticationService;
import com.Springboot.StudentCoureReviewApplication.util.MockMvcUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@WebMvcTest(value = AuthenticationController.class)
class AuthenticationControllerTest {
    @Autowired
    MockMvc mockMvc;

    @MockBean
    private AuthenticationService authenticationService;

    @Test
    @DisplayName("successSignupControllerTest")
    void testSignUpController() throws Exception {
        String URI = "/auth/signUp";

        SignUpDto mockSignUpDto = new SignUpDto();
        mockSignUpDto.setStudentName("Anuj Sundriyal");
        mockSignUpDto.setStudentCity("Kanpur nagar");
        mockSignUpDto.setLoginRole("student");
        mockSignUpDto.setLoginEmail("anuj@Coditas.com");
        mockSignUpDto.setLoginPassword("anuj@1111");

        LoginEntity mockRegisterUser = new LoginEntity();
        mockRegisterUser.setLoginEmail(mockSignUpDto.getLoginEmail());
        mockRegisterUser.setLoginPassword(mockSignUpDto.getLoginPassword());
        mockRegisterUser.setLoginRole(mockSignUpDto.getLoginRole());

        //your input to json
        String inputJson = mapToJson(mockSignUpDto);

        //stub
        when(authenticationService.signUp(mockSignUpDto)).thenReturn(mockRegisterUser);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI) // rest method
                .accept(MediaType.APPLICATION_JSON).content(inputJson) // actual input
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(201, status);

        String content = mvcResult.getResponse().getContentAsString();
        assertEquals(content, "USER REGISTERED !");
    }

    private String mapToJson(Object object) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(object);
    }
}