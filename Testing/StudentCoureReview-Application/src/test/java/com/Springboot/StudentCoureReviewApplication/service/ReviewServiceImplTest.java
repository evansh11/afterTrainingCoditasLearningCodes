package com.Springboot.StudentCoureReviewApplication.service;

import com.Springboot.StudentCoureReviewApplication.dto.request.RemoveReviewDto;
import com.Springboot.StudentCoureReviewApplication.dto.request.ReviewDto;
import com.Springboot.StudentCoureReviewApplication.entities.CourseEntity;
import com.Springboot.StudentCoureReviewApplication.entities.ReviewEntity;
import com.Springboot.StudentCoureReviewApplication.entities.StudentEntity;
import com.Springboot.StudentCoureReviewApplication.repository.CourseRepository;
import com.Springboot.StudentCoureReviewApplication.repository.ReviewRepository;
import com.Springboot.StudentCoureReviewApplication.repository.StudentRepository;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ReviewServiceImplTest {

    @InjectMocks
    ReviewServiceImpl reviewService;

    @Mock
    private ReviewRepository reviewRepository;
    @Mock
    private StudentRepository studentRepository;
    @Mock
    private CourseRepository courseRepository;

    @Test
    @DisplayName("AddReviewTest")
    void addReview() {
        ReviewDto reviewDto = new ReviewDto(1101L, 1109L, 8.7, "Bhut accha");
        StudentEntity student = new StudentEntity(11109L, "Devansh Sharma", "Kanpur", "devansh@Codiats.com", new ArrayList<>(), new ArrayList<>());
//        CourseEntity course = new CourseEntity(101L ,"JAVA" ,12000.0,
//                Stream.of(student).collect(Collectors.toList()),Stream.of(review).collect(Collectors.toList()));
//        ReviewEntity review = new ReviewEntity(11L,6.8,"Recommended",course,student);
        CourseEntity course = new CourseEntity();
        course.setCourseId(11L);
        course.setCourseName("JAVA");
        course.setCoursePrice(12000.90);


        ReviewEntity review = new ReviewEntity();
        review.setReviewId(11101L);
        review.setReviewRating(7.8);
        review.setDescription("Recommended");
        review.setCourse(course);
        review.setStudent(student);

        //setting course list
//        course.getReviewList().add(review);
        //stubbing
        when(studentRepository.findById(Mockito.any())).thenReturn(Optional.of(student));
        when(courseRepository.findById(Mockito.any())).thenReturn(Optional.of(course));

        when(reviewRepository.save(review)).thenReturn(review);
        when(studentRepository.save(student)).thenReturn(student);
        when(courseRepository.save(course)).thenReturn(course);

        assertEquals(0, reviewService.addReview(reviewDto));
    }

    @Test
    @DisplayName("removeReviewTest")
    void removeReview() {
        ReviewEntity review = new ReviewEntity(111L, 7.8, "Noice", null, null);
        when(reviewRepository.findById(Mockito.any())).thenReturn(Optional.of(review));
        doNothing().when(reviewRepository).delete(review);

        RemoveReviewDto removeReviewDto = new RemoveReviewDto();
        removeReviewDto.setReviewId(review.getReviewId());
        assertEquals(true, reviewService.removeReview(removeReviewDto));
    }

    @Test
    @DisplayName("ShowReviewsGivenByStudent")
    void showReviewGivenByStudent() {

    }
}