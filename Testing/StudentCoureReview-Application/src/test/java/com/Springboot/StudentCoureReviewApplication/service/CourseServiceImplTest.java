package com.Springboot.StudentCoureReviewApplication.service;
import com.Springboot.StudentCoureReviewApplication.dto.request.CourseDto;
import com.Springboot.StudentCoureReviewApplication.entities.CourseEntity;
import com.Springboot.StudentCoureReviewApplication.repository.CourseRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CourseServiceImplTest {

    @Mock
    CourseRepository courseRepository;

    @InjectMocks
    CourseServiceImpl courseService;

    @Test
    @DisplayName("registerCourseTest")
    void registerCourseTest() {

        CourseEntity course = new CourseEntity(101L, "DSA WITH JAVA", 1200.0, null, null);
        when(courseRepository.save(course)).thenReturn(course);
        assertEquals(course, courseService.registerCourse(course));
    }

    @Test
    @DisplayName("updateCourseTest")
    void updateCourseTest() {
        CourseDto updateCourse = new CourseDto(101L, "JAVA", 12000.0);
        CourseEntity course = new CourseEntity(101L, "DSA WITH JAVA", 1200.0, null, null);
        //updating
        course.setCourseName(updateCourse.getCourseName());
        course.setCoursePrice(updateCourse.getCoursePrice());
        //stubbing
        when(courseRepository.findById(course.getCourseId())).thenReturn(Optional.of(course));
        when(courseRepository.save(course)).thenReturn(course);
        CourseEntity updatedCourse = courseService.updateCourse(course);
        assertEquals(true, (updatedCourse.getCourseName().equals(updateCourse.getCourseName()) == true ? true : false));
    }

    @Test
    @DisplayName("deleteCourseTest")
    void deleteCourse() {
        CourseEntity course = new CourseEntity(101L, "DSA WITH JAVA", 1200.0, null, null);
        when(courseRepository.findById(course.getCourseId())).thenReturn(Optional.of(course));
        doNothing().when(courseRepository).delete(course);
        assertEquals(true,courseService.deleteCourse(course.getCourseId()));
    }

    @Test
    @DisplayName("showCourseTest")
    void showCourseList() {
        when(courseRepository.findAll()).thenReturn(
                Stream.of(
                        new CourseEntity(101L, "JAVA", 120.0, null, null),
                        new CourseEntity(102L, "SQL", 140.0, null, null)
                ).collect(Collectors.toList()));
        assertEquals(2, courseService.showCourseList().size());
    }
}