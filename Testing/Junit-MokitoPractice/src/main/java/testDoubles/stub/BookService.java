package testDoubles.stub;
import java.util.List;
public class BookService {
    private BookRepository bookRepository;

    public BookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }
    public List<Book> getDiscountedBooks(int discountRate , int day){
        List<Book> newBooks = bookRepository.getBooks(day);
        for(Book book : newBooks){
            double price = book.getPrice();
            double newPrice = price - (discountRate * price /100);
            book.setPrice(newPrice);
        }
        return newBooks;
    }
}
