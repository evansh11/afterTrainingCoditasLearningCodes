package testDoubles.stub;

import java.util.Collection;
import java.util.List;

public interface BookRepository {
  List<Book> getBooks(int day);
}
