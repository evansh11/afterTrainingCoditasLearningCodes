package testDoubles.mock;

public interface BookRepository {
  void save(Book book);
}
