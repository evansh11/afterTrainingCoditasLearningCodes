package testDoubles.dummy;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Book {
    private Long bookId;
    private String bookName;

    public Book(Long bookId, String bookName) {
        this.bookId = bookId;
        this.bookName = bookName;
    }
}
