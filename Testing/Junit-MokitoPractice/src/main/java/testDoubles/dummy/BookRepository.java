package testDoubles.dummy;

import java.util.Collection;

public interface BookRepository {
    void save(Book book);
    Collection<Book> getAllBooks();
}
