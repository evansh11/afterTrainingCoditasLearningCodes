package annotaionsSupport;

import java.util.List;

public interface BookRepository {
  List<Book> getBooks(int day);
}
