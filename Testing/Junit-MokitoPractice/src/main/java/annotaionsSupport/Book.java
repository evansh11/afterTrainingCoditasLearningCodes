package annotaionsSupport;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
public class Book {
    private Long bookId;
    private String bookName;
    private double price;
    private LocalDate date;

    public Book(Long bookId, String bookName, double price, LocalDate date) {
        this.bookId = bookId;
        this.bookName = bookName;
        this.price = price;
        this.date = date;
    }
}
