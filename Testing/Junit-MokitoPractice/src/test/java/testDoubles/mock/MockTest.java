package testDoubles.mock;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.LocalDate;

import static org.mockito.Mockito.mock;

public class MockTest {
    @Test
    public void demoMock(){
        BookRepository bookRepository = mock(BookRepository.class);
        BookService bookService = new BookService(bookRepository);

        Book book1 =new  Book(1234L, "MOckito" ,1120, LocalDate.now());
        Book book2 =new Book(1235L, "MOckito-II" ,129, LocalDate.now());

        bookService.addBook(book1);// return more than 400
        bookService.addBook(book2); // actually save called

        Mockito.verify(bookRepository).save(book2);
        Mockito.verify(bookRepository).save(book1);
    }
}
