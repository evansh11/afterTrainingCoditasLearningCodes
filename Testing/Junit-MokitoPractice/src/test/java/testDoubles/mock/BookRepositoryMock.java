package testDoubles.mock;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class BookRepositoryMock implements BookRepository {
    int saveCalled = 0;
      Book lastAddedBook = null;
    @Override
    public void save(Book book) {
        saveCalled++;
        lastAddedBook = book;
    }
    public int timesCalled(){
        return saveCalled;
    }
    public Book getLastAddedBook(){
        return lastAddedBook;
    }

    public void verify(Book book,int times){
        assertEquals(times,saveCalled);
        assertEquals(book,lastAddedBook);
    }
}
