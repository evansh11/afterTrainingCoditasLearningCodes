package testDoubles.fake;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class FakeBookRepository implements BookRepository{
    private Map<Long,Book> books = new HashMap<>();
    @Override
    public void save(Book book) {
        books.put(book.getBookId(),book);
    }

    @Override
    public Collection<Book> getAllBooks() {
        return books.values();
    }
}
