package testDoubles.fake;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

public class BookServiceFakeTest {


    @Test
    void addBook() {
        BookRepository bookRepository = new FakeBookRepository();
        BookService bookService = new BookService(bookRepository);
        bookService.addBook(new Book(123L, "Mockito"));
        bookService.addBook(new Book(124L, "Junit"));
        assertEquals(2, bookService.findNumberOfBook());
    }

    @Test
    public void testFakeWithMockito() {

        //fake bookRepository
        BookRepository bookRepository1 = Mockito.mock(BookRepository.class);
        BookService bookService = new BookService(bookRepository1);

        Book book1 = new Book(123L, "Mockito");
        Book book2 = new Book(124L, "Junit");

        Collection<Book> books = new ArrayList<>();
        books.add(book1);
        books.add(book2);
        
        when(bookRepository1.getAllBooks()).thenReturn(books);
        assertEquals(2, bookService.findNumberOfBook());
    }
}
