package testDoubles.stub;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class BookRepositoryStub implements BookRepository {
    @Override
    public List<Book> getBooks(int day) {
        return Stream.of(new Book(101l,"HC VERMA",100, LocalDate.now()),
                        new Book(102l,"RD VERMA",1000,LocalDate.now()),
                        new Book(103l,"DEV VERMA",180,LocalDate.now())
                ).collect(Collectors.toList());
    }
}
