package testDoubles.stub;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

class BookServiceTest {

    @Test
    void demoStubGetDiscountedBooks() {
        BookRepository bookRepository = new BookRepositoryStub();
        BookService bookService = new BookService(bookRepository);

        List<Book> newBookWithDiscount = bookService.getDiscountedBooks(10,7);
        assertEquals(3,newBookWithDiscount.size());
        //checking after discount book1 price
        assertEquals(90,newBookWithDiscount.get(0).getPrice());
        //book2
        assertEquals(900,newBookWithDiscount.get(1).getPrice());
        //book3
        assertEquals(162,newBookWithDiscount.get(2 ).getPrice());
    }

    @Test
    void testStubGetDiscountedBooksWithMockito(){
        BookRepository bookRepository = Mockito.mock(BookRepository.class);
        BookService bookService = new BookService(bookRepository);

        when(bookRepository.getBooks(7)).thenReturn(Stream.of(
                new Book(101l,"HC VERMA",100, LocalDate.now()),
                new Book(102l,"RD VERMA",1000,LocalDate.now()),
                new Book(103l,"DEV VERMA",180,LocalDate.now())
        ).collect(Collectors.toList()));

        List<Book> newBookWithDiscount = bookService.getDiscountedBooks(10,7);
        //testing number of discounted number of books
        assertEquals(3,newBookWithDiscount.size());
        //checking after discount book1 price
        assertEquals(90,newBookWithDiscount.get(0).getPrice());
        //book2
        assertEquals(900,newBookWithDiscount.get(1).getPrice());
        //book3
        assertEquals(162,newBookWithDiscount.get(2 ).getPrice());
    }
}