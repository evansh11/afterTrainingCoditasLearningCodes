package testDoubles.spy;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SpyTest {
    @Test
    public void demoSpy(){
        BookRepositorySpy bookRepositorySpy = new BookRepositorySpy();
        BookService bookService = new BookService(bookRepositorySpy);
        Book book1 =new Book(1234L, "MOckito" ,120, LocalDate.now());
        Book book2 =new Book(1235L, "MOckito-II" ,129, LocalDate.now());

        bookService.addBook(book1);
        bookService.addBook(book2);

        assertEquals(2,bookRepositorySpy.saveCalled);
        assertEquals(book2,bookRepositorySpy.getLastAddedBook());
    }
}
