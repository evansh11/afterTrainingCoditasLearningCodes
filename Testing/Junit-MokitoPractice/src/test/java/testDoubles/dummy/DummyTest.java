package testDoubles.dummy;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

public class DummyTest {
    BookRepository bookRepository = new FakeBookRepository();
    EmailService emailService = new DummyEmailService();
    BookService bookService = new BookService(bookRepository, emailService);

    @Test
    void dummyDemo() {
        bookService.addBook(new Book(123L, "Mockito"));
        bookService.addBook(new Book(124L, "Junit"));
        assertEquals(2, bookService.findNumberOfBook());
    }

    @Test
    void dummyDemoWithMockito() {

        //fake bookRepository
        BookRepository bookRepository1 = Mockito.mock(BookRepository.class);
        EmailService emailService1 = Mockito.mock(EmailService.class);
        BookService bookService = new BookService(bookRepository1, emailService1);

        Book book1 = new Book(123L, "Mockito");
        Book book2 = new Book(124L, "Junit");

        Collection<Book> books = new ArrayList<>();
        books.add(book1);
        books.add(book2);

        when(bookRepository1.getAllBooks()).thenReturn(books);
        assertEquals(2, bookService.findNumberOfBook());
    }
}