package testDoubles.dummy;

import testDoubles.dummy.Book;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class FakeBookRepository implements BookRepository {

    // in memory database ,list ,hashmap
    Map<Long,Book> bookStore = new HashMap<>();
    @Override
    public void save(Book book) {
        bookStore.put(book.getBookId(),book);
    }

    @Override
    public Collection<Book> getAllBooks() {
        return bookStore.values();
    }
}
