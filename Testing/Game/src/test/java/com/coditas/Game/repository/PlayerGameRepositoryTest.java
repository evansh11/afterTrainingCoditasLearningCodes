package com.coditas.Game.repository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class PlayerGameRepositoryTest {
    @Autowired
    PlayerGameRepository playerGameRepository;

    @Test
    void getListOfPlayerInGame() {
        assertThat(playerGameRepository.getListOfPlayerInGame(2L)).isNotEmpty();
    }
}