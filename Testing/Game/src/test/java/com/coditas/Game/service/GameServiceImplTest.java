package com.coditas.Game.service;

import com.coditas.Game.entities.Game;
import com.coditas.Game.entities.Player;
import com.coditas.Game.model.request.StartGameDto;
import com.coditas.Game.repository.GameRepository;
import com.coditas.Game.repository.PlayerRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.internal.util.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockBeans;
import org.springframework.test.context.event.annotation.PrepareTestInstance;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.MockitoAnnotations.initMocks;

@ExtendWith(MockitoExtension.class)
class GameServiceImplTest {

    @MockBean
    private PlayerRepository playerRepository;
    @MockBean
    GameRepository gameRepository;

    @InjectMocks
    GameServiceImpl gameService;

    @BeforeEach
    private void init(){
        initMocks(this);
    }
    @Test
    void testStartGame() {
        //player1
        Player player = new Player();
        player.setPlayerId(4L);
        player.setPlayerStatus(false);
        player.setPlayerName("p1");

        StartGameDto startGameDto = new StartGameDto();
        startGameDto.setGameId(1l);
        startGameDto.setHp(100L);
        startGameDto.setPlayerId1(2L);
        startGameDto.setPlayerId2(2L);

        Game game =new Game();
        game.setGameId(1l);
        game.setGameStatus(false);
        game.setGameName("ava");

        Mockito.when(playerRepository.findById(anyLong())).thenReturn(Optional.of(player));
        Mockito.when(playerRepository.findById(anyLong())).thenReturn(Optional.of(player));
        Mockito.when(gameRepository.findById(anyLong())).thenReturn(Optional.of(game));
        String s = gameService.startGame(startGameDto);
        Assertions.assertEquals("No",s);
    }
}